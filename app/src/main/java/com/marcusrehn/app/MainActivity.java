package com.marcusrehn.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.marcusrehn.app.database.ItemsFragment;

public final class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {



    public static final int NAV_SHOPPING_LIST = 0;
    public static final int NAV_ITEMS = 1;
    public static final int NAV_STATISTICS = 2;
    public static final int NAV_SETTINGS = 3;
    public static final int NAV_ABOUT = 4;

    ShoppingListFragment shoppingListFragment;
    StatisticsFragment statisticsFragment;
    SettingFragment settingFragment;
    AboutFragment aboutFragment;
    ItemsFragment itemsFragment;


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        shoppingListFragment = new ShoppingListFragment();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(shoppingListFragment == null)
            shoppingListFragment = new ShoppingListFragment();

        switch (position){
            case NAV_SHOPPING_LIST:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, shoppingListFragment)
                        .commit();
                //fragmentManager.beginTransaction().show(shoppingListFragment);
                mTitle = "The shopping list";
                break;
            case NAV_ITEMS:
                if(itemsFragment == null)
                    itemsFragment = new ItemsFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, itemsFragment)
                        .commit();
                mTitle = getText(R.string.items_label);
                break;
            case NAV_STATISTICS:
                if(statisticsFragment == null)
                    statisticsFragment = new StatisticsFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, statisticsFragment)
                        .commit();
                mTitle = getText(R.string.stats_label);
                break;
            case NAV_SETTINGS:
                if(settingFragment == null)
                    settingFragment =  new SettingFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, settingFragment)
                        .commit();
                mTitle = getText(R.string.settings_label);
                break;
            case NAV_ABOUT:
                if(aboutFragment == null)
                    aboutFragment = new AboutFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, aboutFragment)
                        .commit();
                mTitle = getText(R.string.about_label);
                break;
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.


            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }


}
