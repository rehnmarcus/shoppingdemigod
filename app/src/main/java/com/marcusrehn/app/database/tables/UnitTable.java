package com.marcusrehn.app.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marcusrehn.app.database.DatabaseHelper;
import com.marcusrehn.app.database.holders.UnitHolder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by rehnen on 2014-02-06.
 */
public class UnitTable {

    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    private String[] columns = {
            dbHelper.UNIT_ID,
            dbHelper.UNIT_NAME
    };

    public UnitTable(Context context){
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }

    public UnitHolder select(long id){
        Cursor cursor = db.query(dbHelper.TABLE_ITEM, columns, dbHelper.ITEM_ID + "=" + id, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() <= 0){
            return null;
        }else{
            return new UnitHolder(cursor.getLong(0), cursor.getString(1));
        }
    }

    public ArrayList<UnitHolder> selectAll(){
        ArrayList<UnitHolder> allUnits = new ArrayList<UnitHolder>();
        Cursor cursor = db.query(DatabaseHelper.TABLE_ITEM, columns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            UnitHolder unit = new UnitHolder(cursor.getLong(0), cursor.getString(1));
            allUnits.add(unit);
        }
        return allUnits;
    }
    public void insert(UnitHolder unitHolder){
        ContentValues values = new ContentValues();
        values.put(dbHelper.UNIT_ID, unitHolder.getId());
        values.put(dbHelper.UNIT_NAME, unitHolder.getName());
        db.insert(dbHelper.TABLE_UNIT, null, values);
    }

    public void delete(UnitHolder unitHolder){
        db.delete(dbHelper.TABLE_UNIT, dbHelper.UNIT_ID + " = " + unitHolder.getId(), null);
    }

    public void recreate(){
        dbHelper.recreateUnit(db);
    }


}
