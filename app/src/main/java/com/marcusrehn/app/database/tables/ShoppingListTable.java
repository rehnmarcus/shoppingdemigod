package com.marcusrehn.app.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.marcusrehn.app.database.DatabaseHelper;
import com.marcusrehn.app.database.holders.ShoppingListItemHolder;

import java.util.ArrayList;

/**
 * Created by rehnen on 2014-02-06.
 */
public class ShoppingListTable {

    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    private String[] columns = {
        dbHelper.SHOPPING_LIST_ID,
        dbHelper.SHOPPING_LIST_ITEM_ID,
        dbHelper.SHOPPING_LIST_DONE
    };

    public ShoppingListTable(Context context){
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLiteException{
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }

    public ShoppingListItemHolder select(long id){
        Cursor cursor = db.query(dbHelper.TABLE_SHOPPING_LIST, columns, dbHelper.SHOPPING_LIST_ITEM_ID + " = " + id, null, null, null, null);
        if(cursor.getCount() <= 0){
            return null;
        }else {
            cursor.moveToFirst();
            return new ShoppingListItemHolder(cursor.getLong(0), cursor.getLong(1), (cursor.getInt(2) > 0));
        }
    }

    public ArrayList<ShoppingListItemHolder> selectAll(){
        ArrayList<ShoppingListItemHolder> allItems = new ArrayList<ShoppingListItemHolder>();
        Cursor cursor =  db.query(dbHelper.TABLE_SHOPPING_LIST, columns, null, null, null, null,null);
        if(cursor.getCount() < 1)
            return allItems;
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            allItems.add(new ShoppingListItemHolder(cursor.getLong(0),
                    cursor.getLong(1),
                    (cursor.getLong(2))> 0));
            cursor.moveToNext();
        }
        return allItems;
    }

    public long insert(ShoppingListItemHolder itemHolder){
        ContentValues values = new ContentValues();
        //values.put(dbHelper.SHOPPING_LIST_ID, itemHolder.getId());
        values.put(dbHelper.SHOPPING_LIST_ITEM_ID, itemHolder.getItemId());
        values.put(dbHelper.SHOPPING_LIST_DONE, (itemHolder.isBought()? 1:0));

        return db.insert(dbHelper.TABLE_SHOPPING_LIST, null, values);
    }

    public void update(ShoppingListItemHolder itemHolder){
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.SHOPPING_LIST_ID, itemHolder.getId());
        cv.put(dbHelper.SHOPPING_LIST_ITEM_ID, itemHolder.getItemId());
        cv.put(dbHelper.SHOPPING_LIST_DONE, itemHolder.isBought());

        db.update(dbHelper.TABLE_SHOPPING_LIST, cv, dbHelper.SHOPPING_LIST_ID + " = " + itemHolder.getId(), null);
        //db.delete(dbHelper.TABLE_SHOPPING_LIST, dbHelper.SHOPPING_LIST_ID + " = " + itemHolder.getId(), null);
    }

    public void delete(long l){
        db.delete(dbHelper.TABLE_SHOPPING_LIST, dbHelper.SHOPPING_LIST_ID + " = " + l, null);
    }
    public void deleteSelected(){
        db.delete(dbHelper.TABLE_SHOPPING_LIST, dbHelper.SHOPPING_LIST_DONE + " = " +true, null);
    }

    public void recreate(){
        dbHelper.recreateShoppingList(db);
    }

}
