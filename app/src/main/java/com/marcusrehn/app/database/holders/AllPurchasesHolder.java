package com.marcusrehn.app.database.holders;

/**
 * Created by rehnen on 2014-02-06.
 */
public class AllPurchasesHolder {

    long id;
    long itemId;
    String timeStamp;

    public AllPurchasesHolder(long id, long itemId, String timeStamp) {
        this.id = id;
        this.itemId = itemId;
        this.timeStamp = timeStamp;
    }

    public AllPurchasesHolder(long itemId, String timeStamp) {
        this.itemId = itemId;
        this.timeStamp = timeStamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
