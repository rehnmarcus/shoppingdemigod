package com.marcusrehn.app.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.marcusrehn.app.database.DatabaseHelper;
import com.marcusrehn.app.database.holders.AllPurchasesCount;
import com.marcusrehn.app.database.holders.AllPurchasesHolder;
import com.marcusrehn.app.database.holders.ShoppingListItemHolder;

import java.util.ArrayList;

/**
 * Created by rehnen on 2014-02-06.
 */
public class AllPurchasesTable {

    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    private String[] columns = {
            dbHelper.HISTORY_ID,
            dbHelper.HISTORY_ITEM_ID,
            dbHelper.HISTORY_TIMESTAMP
    };

    public AllPurchasesTable(Context context){
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLiteException{
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }

    public AllPurchasesHolder select(long id){
        Cursor cursor = db.query(dbHelper.TABLE_SHOPPING_LIST, columns, dbHelper.SHOPPING_LIST_ID + "=" + id, null, null, null, null);
        cursor.moveToFirst();
        if(cursor.getCount() <= 0){
            return null;
        }else {
            return new AllPurchasesHolder(cursor.getLong(0), cursor.getLong(1), cursor.getString(2));
        }
    }
    public ArrayList<AllPurchasesHolder> selectAll(){
        ArrayList<AllPurchasesHolder> allItems = new ArrayList<AllPurchasesHolder>();
        Cursor cursor =  db.query(dbHelper.TABLE_ALL_PURCHASES, columns, null, null, null, null,null);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                allItems.add(new AllPurchasesHolder(cursor.getLong(0),
                    cursor.getLong(1),
                    cursor.getString(2)));
                Log.d("dorp", "what the fuck");
                cursor.moveToNext();
            }
        }
        return allItems;
    }

    public ArrayList<AllPurchasesCount> selectMostCommon(int count){
        ArrayList<AllPurchasesCount> items = new ArrayList<AllPurchasesCount>();
        String searchStr = "SELECT " + dbHelper.HISTORY_ITEM_ID + ", count(" + dbHelper.HISTORY_ITEM_ID + ") as derp\n" +
                "FROM "+ dbHelper.TABLE_ALL_PURCHASES +"\n" +
                "group by "+ dbHelper.HISTORY_ITEM_ID + "\n" +
                " order by derp desc " +
                "limit " + count;

        Cursor cursor = db.rawQuery(searchStr, new String[]{});
        if(cursor == null)
            return items;
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            items.add(new AllPurchasesCount(cursor.getLong(0), cursor.getLong(1)));
            cursor.moveToNext();
        }
        return items;
    }

    public ArrayList<AllPurchasesHolder> selectLatest(int days, int limit){
        ArrayList<AllPurchasesHolder> items = new ArrayList<AllPurchasesHolder>();
        String query = "SELECT " + dbHelper.HISTORY_ID + ", " + dbHelper.HISTORY_ITEM_ID +", " + dbHelper.HISTORY_TIMESTAMP + " \n" +
                "FROM " + dbHelper.TABLE_ALL_PURCHASES + "\n" +
                "WHERE "+ dbHelper.HISTORY_TIMESTAMP + " < date('now', '-" + days + " day')\n" +
                "OrDER BY " + dbHelper.HISTORY_TIMESTAMP + " DESC LIMIT " + limit;
        Cursor cursor = db.rawQuery(query, new String[]{});

        if(cursor == null)
            return items;
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            items.add(new AllPurchasesHolder(cursor.getLong(0), cursor.getLong(1), cursor.getString(2)));
            cursor.moveToNext();
        }
        return items;

    }

    public void insert(AllPurchasesHolder itemHolder){
        ContentValues values = new ContentValues();
        //values.put(dbHelper.HISTORY_ID, itemHolder.getId());
        values.put(dbHelper.HISTORY_ITEM_ID, itemHolder.getItemId());
        //values.put(dbHelper.HISTORY_TIMESTAMP, itemHolder.getTimeStamp());
        db.insert(dbHelper.TABLE_ALL_PURCHASES, null, values);
    }

    public void delete(ShoppingListItemHolder itemHolder){
        db.delete(dbHelper.TABLE_ALL_PURCHASES, dbHelper.HISTORY_ID + " = " + itemHolder.getId(), null);
    }

    public void recreate(){
        dbHelper.recreateAllPurchases(db);
    }

}
