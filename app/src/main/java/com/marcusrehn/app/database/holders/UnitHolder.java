package com.marcusrehn.app.database.holders;

/**
 * Created by rehnen on 2014-02-06.
 */
public class UnitHolder {
    private long id;
    private String name;


    public UnitHolder(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
