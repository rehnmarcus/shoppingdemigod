package com.marcusrehn.app.database.holders;

/**
 * Created by rehnen on 2014-03-29.
 */
public class AllPurchasesCount {

    private long id;
    private long count;

    public AllPurchasesCount(long id, long count) {
        this.id = id;
        this.count = count;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
