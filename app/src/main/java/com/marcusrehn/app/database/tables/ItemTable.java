package com.marcusrehn.app.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.marcusrehn.app.database.DatabaseHelper;
import com.marcusrehn.app.database.holders.ItemHolder;

import java.util.ArrayList;

/**
 * Created by rehnen on 2014-02-06.
 */
public class ItemTable {

    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;
    private String[] columns = {
            dbHelper.ITEM_ID,
            dbHelper.ITEM_NAME,
            dbHelper.ITEM_COMMENT,
            dbHelper.ITEM_AMOUNT,
            dbHelper.ITEM_PICTURE
    };

    public ItemTable(Context context){
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLiteException{
        db = dbHelper.getWritableDatabase();
    }
    public void close(){
        dbHelper.close();
    }
    public ItemHolder selectItem(long id){
        Cursor cursor = db.query(dbHelper.TABLE_ITEM, columns, dbHelper.ITEM_ID + " = " + id, null, null, null, null);
        cursor.moveToFirst();

        if(cursor.getCount() <= 0){
            return null;
        }else{
            return new ItemHolder(cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4));
        }
    }

    public ItemHolder selectItem(String name){
        String args[] = {name};
        Cursor cursor = db.rawQuery("SELECT * FROM " + dbHelper.TABLE_ITEM + " WHERE " + dbHelper.ITEM_NAME + " like ?", args);
        //cursor = db.query(dbHelper.TABLE_ITEM, columns, dbHelper.ITEM_NAME + " = " + name, null, null, null, null);


        if(cursor.getCount() == 0){
            return null;
        }else{
            cursor.moveToFirst();
            return new ItemHolder(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4));
        }
    }

    public long insertItem(ItemHolder ih){
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.ITEM_NAME, ih.getName());

        //db.execSQL("");
        long insertId = db.insert(dbHelper.TABLE_ITEM, null, values);

        return insertId;

    }
    public ArrayList<ItemHolder> selectAll(){
        ArrayList<ItemHolder> allItems = new ArrayList<ItemHolder>();
        Cursor cursor = db.query(DatabaseHelper.TABLE_ITEM, columns, null, null, null, null, null);

        if(cursor.getCount() < 1)
            return allItems;

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()){
            ItemHolder item = new ItemHolder(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            allItems.add(item);
            cursor.moveToPrevious();
        }
        cursor.close();
        return allItems;
    }
    public ArrayList<ItemHolder> selectAllSorted(){
        ArrayList<ItemHolder> allItems = new ArrayList<ItemHolder>();
        Cursor cursor = db.query(DatabaseHelper.TABLE_ITEM, columns, null, null, null, null, DatabaseHelper.ITEM_NAME + " DESC");

        if(cursor.getCount() < 1)
            return allItems;

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()){
            ItemHolder item = new ItemHolder(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            allItems.add(item);
            cursor.moveToPrevious();
        }
        cursor.close();
        return allItems;
    }

    public void update(ItemHolder ih){
        ContentValues cv = new ContentValues();
        cv.put(dbHelper.ITEM_NAME, ih.getName());
        cv.put(dbHelper.ITEM_AMOUNT, ih.getAmount());
        cv.put(dbHelper.ITEM_COMMENT, ih.getComment());
        cv.put(dbHelper.ITEM_PICTURE, ih.getPicture());

        db.update(dbHelper.TABLE_ITEM, cv, dbHelper.ITEM_ID + " = " + ih.getId(), null);
    }

    public int deleteItem(ItemHolder itemHolder){
        return db.delete(dbHelper.TABLE_ITEM, dbHelper.ITEM_ID + " = " + itemHolder.getId(), null);
    }

    public void remakeTable(){
        dbHelper.recreateItem(db);
    }
}
