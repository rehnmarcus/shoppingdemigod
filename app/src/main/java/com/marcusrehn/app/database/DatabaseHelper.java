package com.marcusrehn.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rehnen on 2014-02-06.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // This class in in charge of creating the tables
    // and also of keeping track of the names of the tables and their columns

    public static final int DB_VERSION = 9;

    public static final String DB_NAME = "rehnen_shopping_list.db";
    public static final String TABLE_ITEM = "item";
    public static final String TABLE_SHOPPING_LIST = "shopping_list";
    public static final String TABLE_UNIT = "unit";
    public static final String TABLE_ALL_PURCHASES = "purchases";

    //item
    public static final String ITEM_ID      = "id";
    public static final String ITEM_NAME    = "name";
    public static final String ITEM_COMMENT = "comment";
    public static final String ITEM_AMOUNT  = "amount";
    public static final String ITEM_PICTURE = "pic";


    //shopping list
    public static final String SHOPPING_LIST_ID         = "id";
    public static final String SHOPPING_LIST_ITEM_ID    = "fk_item_id";
    public static final String SHOPPING_LIST_DONE  = "done";


    //History
    public static final String HISTORY_ID           = "id";
    public static final String HISTORY_ITEM_ID      = "fk_item_id";
    public static final String HISTORY_COMMENT      = "comment";
    public static final String HISTORY_TIMESTAMP    = "time_of_day";
    //public static final String HISTORY_LIST_ID = "fk_list_id"; //should be increased every time the current shopping list is dumped to history

    public static final String UNIT_ID = "id";
    public static final String UNIT_NAME = "name";

    public static final String CREATE_ITEM = "CREATE TABLE " + TABLE_ITEM + "(" +
            ITEM_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            ITEM_NAME + " TEXT UNIQUE, " +
            ITEM_COMMENT + " TEXT, "+
            ITEM_AMOUNT + " TEXT, "+
            ITEM_PICTURE + " TEXT);";

    public static final String CREATE_SHOPPING_LIST = "CREATE TABLE " + TABLE_SHOPPING_LIST + "(" +
            SHOPPING_LIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            SHOPPING_LIST_ITEM_ID +" INTEGER UNIQUE, " +
            SHOPPING_LIST_DONE + " BOOL ); ";

    public static final String CREATE_ALL_PURCHASES = "CREATE TABLE " + TABLE_ALL_PURCHASES + "(" +
            HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            HISTORY_ITEM_ID +" INTEGER, " +
            HISTORY_TIMESTAMP +" TIMESTAMP NOT NULL DEFAULT current_timestamp " +
            ");";

    public static final String CREATE_UNIT = "CREATE TABLE " + TABLE_UNIT + "(" +
            UNIT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+
            UNIT_NAME + " TEXT)";

    public static final String DROP_ITEM = "DROP TABLE IF EXISTS " + TABLE_ITEM;
    public static final String DROP_SHOPPING_LIST = "DROP TABLE IF EXISTS " + TABLE_SHOPPING_LIST;
    public static final String DROP_ALL_PURCHASES = "DROP TABLE IF EXISTS " + TABLE_ALL_PURCHASES;
    public static final String DROP_UNIT = "DROP TABLE IF EXISTS " + TABLE_UNIT;


    public static final String CREATE_TRIGGER_ON_REMOVE_ITEM =
            " CREATE TRIGGER TRIGGER_ON_REMOVE_ITEM after delete ON "+ TABLE_ITEM + "\n" +
                    "FOR EACH ROW    " +
                    "BEGIN\n" +
                    "      DELETE FROM " + TABLE_ALL_PURCHASES + " WHERE "+ HISTORY_ITEM_ID+ " = OLD.id ;\n" +
                    "      DELETE FROM " + TABLE_SHOPPING_LIST + " WHERE "+ SHOPPING_LIST_ITEM_ID+ " = OLD.id ;\n" +
                    "    END ;";
    public static final String DROP_TRIGGER_ON_REMOVE_ITEM =
            "DROP TRIGGER IF EXISTS TRIGGER_ON_REMOVE_ITEM";


    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        //Create tables
        db.execSQL(CREATE_ITEM);
        db.execSQL(CREATE_SHOPPING_LIST);
        db.execSQL(CREATE_UNIT);
        db.execSQL(CREATE_ALL_PURCHASES);
        db.execSQL(CREATE_TRIGGER_ON_REMOVE_ITEM);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TRIGGER_ON_REMOVE_ITEM);
        db.execSQL(DROP_ITEM);
        db.execSQL(DROP_SHOPPING_LIST);
        db.execSQL(DROP_ALL_PURCHASES);
        db.execSQL(DROP_UNIT);
        onCreate(db);
    }

    public void recreateItem(SQLiteDatabase db){
        db.execSQL(DROP_ITEM);
        db.execSQL(CREATE_ITEM);
    }
    public void recreateShoppingList(SQLiteDatabase db){
        db.execSQL(DROP_SHOPPING_LIST);
        db.execSQL(CREATE_SHOPPING_LIST);
    }
    public void recreateAllPurchases(SQLiteDatabase db){
        db.execSQL(DROP_ALL_PURCHASES);
        db.execSQL(CREATE_ALL_PURCHASES);
    }
    public void recreateUnit(SQLiteDatabase db){
        db.execSQL(DROP_UNIT);
        db.execSQL(CREATE_UNIT);
    }


}
