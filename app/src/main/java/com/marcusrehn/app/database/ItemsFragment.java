package com.marcusrehn.app.database;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marcusrehn.app.Consts;
import com.marcusrehn.app.ItemSettingsActivity;
import com.marcusrehn.app.R;
import com.marcusrehn.app.database.holders.AllPurchasesHolder;
import com.marcusrehn.app.database.holders.ItemHolder;
import com.marcusrehn.app.database.tables.AllPurchasesTable;
import com.marcusrehn.app.database.tables.ItemTable;

import java.util.ArrayList;

/**
 * Created by rehnen on 2014-04-18.
 */
public class ItemsFragment extends Fragment implements View.OnClickListener{

    LinearLayout llItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_items, container, false);
        llItems = (LinearLayout) v.findViewById(R.id.ll_items);
        initItems();
        return v;
    }

    private void initItems(){
        ItemTable itemTable = new ItemTable(getActivity().getApplicationContext());
        itemTable.open();
        ArrayList<ItemHolder> ih= itemTable.selectAllSorted();
        itemTable.close();
        //ItemTable itemTable = new ItemTable(getActivity().getApplicationContext());
        //itemTable.open();
        //ArrayList<ItemHolder>itemHolders = itemTable.selectAll();

        if(ih.size() > 0)
            llItems.removeAllViews();
        for(int i = 0; i < ih.size(); i++){

            String itemName = ih.get(i).getName();
            String itemInfo = ih.get(i).getComment();
            long id = ih.get(i).getId();




            if(id > 0)
                adItem(itemName, itemInfo, id);
        }
    }

    private void adItem(String name, String info, long id){

        View row    = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.stats_list_row, null);
        TextView txtName    = (TextView)row.findViewById(R.id.txtV_stats_row_name);
        TextView txtInfo    = (TextView)row.findViewById(R.id.txtV_stats_row_info);
        TextView txtId      = (TextView)row.findViewById(R.id.txtV_stats_row_id);

        txtName.setText(name);
        txtInfo.setText(info);
        txtId.setText(String.valueOf(id));
        row.setOnClickListener(this);
        llItems.addView(row);
    }

    @Override
    public void onClick(View view) {
        TextView txtId = (TextView)view.findViewById(R.id.txtV_stats_row_id);
        Log.d("clicked", "id = " + txtId.getText().toString());
        Intent startItem = new Intent();
        startItem.setClass(getActivity(), ItemSettingsActivity.class);
        startItem.putExtra(Consts.ITEM_ID, Long.valueOf(txtId.getText().toString()));
        startActivity(startItem);
    }
}
