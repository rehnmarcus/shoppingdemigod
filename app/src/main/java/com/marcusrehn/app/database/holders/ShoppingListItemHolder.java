package com.marcusrehn.app.database.holders;

/**
 * Created by rehnen on 2014-02-06.
 */
public class ShoppingListItemHolder {

    private long id;
    private long itemId;
    private boolean bought;

    public ShoppingListItemHolder(long itemId) {
        this.id = 1;
        this.itemId = itemId;
        this.bought = false;
    }

    public ShoppingListItemHolder(long id, long itemId, boolean bought) {
        this.id = id;
        this.itemId = itemId;
        this.bought = bought;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }
}
