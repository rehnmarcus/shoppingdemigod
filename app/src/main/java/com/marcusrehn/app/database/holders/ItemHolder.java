package com.marcusrehn.app.database.holders;

/**
 * Created by rehnen on 2014-02-06.
 */
public class ItemHolder {
    private long id;
    private String name;
    private String comment;
    private String amount;
    private String picture;

    public ItemHolder(String name){
        this.id = id;
        this.name = name;
        this.comment = "";
        this.amount = "";
        this.picture = "";
    }

    public ItemHolder(long id, String name, String comment, String amount) {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.amount = amount;
        this.picture = "";
    }

    public ItemHolder(long id, String name, String comment, String amount, String picture) {
        this.id = id;
        this.name = (name == null ? "" : name);
        this.comment = (comment == null ? "" : comment);
        this.amount = (amount == null ? "" : amount);
        this.picture = (picture == null ? "" : picture);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
