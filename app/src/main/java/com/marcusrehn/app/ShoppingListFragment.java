package com.marcusrehn.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import com.marcusrehn.app.database.holders.*;
import com.marcusrehn.app.database.tables.*;

import java.io.FileInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by rehnen on 2014-01-28.
 */
public class ShoppingListFragment extends Fragment implements View.OnClickListener {

    //Bundleflags
    private static final String TXTV_ITEM_NAME = "item_name";
    // I will most likely only need to save that text as the rest should be stored in the database table
    // By "the rest" I mean the actual shopping list
    //private static final String[] derp = new String[]{"Lel", "katt", "cat", "dog", "cathy", "cat2", "cat3", "cat4", "cat5"};
    private Bundle savedState = null;
    /*Views*/
    private AutoCompleteTextView acTxt;
    private Button btnAddItem;
    private Button btnClear;
    private LinearLayout llItemHolder;
    private LinearLayout llSuggestions;

    /*Tables*/
    private ItemTable itemTable;
    private ShoppingListTable shoppingListTable;
    private AllPurchasesTable allPurchasesTable;


    private ArrayList<ItemHolder> items;
    private ArrayList<String> itemNames;
    private ArrayList<ItemHolder> itemsSuggestions;
    private ArrayList<String> itemNamesSuggestions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_shopping_list, container, false);
        initGUI(v, container);


        if(savedInstanceState != null && savedState == null){

        }

        ActionBar actionBar = getActivity().getActionBar();
        if(Build.VERSION.RELEASE.startsWith("4"))
            //actionBar.setIcon(R.drawable.shoping_cart);

        //-setRetainInstance(true);
        initDB();
        restoreShoppingList();
        return v;

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
           // ActionBar actionBar = getActivity().getActionBar();
            //actionBar.setIcon(R.drawable.shoping_cart);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        super.onResume();
        restoreShoppingList();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveState();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        try{
            itemTable = new ItemTable(getActivity().getApplicationContext());
            itemTable.open();
            ItemHolder ih = itemTable.selectItem(getTxtvItemName());
            long l;
            if(ih == null){
                l = itemTable.insertItem(new ItemHolder(getTxtvItemName()));
                initHints();
            }else{
                l = ih.getId();
            }
            Log.d("flörp", l+ "");
            itemTable = new ItemTable(getActivity().getApplicationContext());
            itemTable.open();
            ArrayList<ItemHolder> allI= itemTable.selectAll();
            for(int i = 0; i < allI.size(); i++)
                Log.d("dorp", allI.get(i).getName());
            itemTable.close();

            shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
            shoppingListTable.open();
            //if(shoppingListTable.select(l) == null){

            ArrayList <ShoppingListItemHolder> shl = shoppingListTable.selectAll();
            boolean alreadyThere = false;
            for(int i = 0; i < shl.size(); i ++){
                Log.d("dorp", shl.get(i).getId() + " - " + shl.get(i).getItemId() + " - " + shl.size());
                if(l == shl.get(i).getItemId()){
                    alreadyThere = true;
                    Log.d("dorp", "now its true");
                }
            }
            if(!alreadyThere){
                shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
                shoppingListTable.open();
                shoppingListTable.insert(new ShoppingListItemHolder(l));
                Log.d("allready", "nah");
                acTxt.setText("");
                insertItem(l);
            }
        }catch (SQLiteException e){
            Log.e("err_mess", e.getMessage());
        }catch (Exception e){
            //Log.e("err_mess", e.getMessage());
            //Log.e("err_mess", e.getLocalizedMessage())
            Log.e("plop", e.toString());
        }
    }


    public void initGUI(View fragment, ViewGroup container){
        this.btnAddItem = (Button) fragment.findViewById(R.id.btn_shopping_list_add_item);
        this.btnClear   = (Button) fragment.findViewById(R.id.btn_shopping_list_clear);
        this.acTxt      = (AutoCompleteTextView)fragment.findViewById(R.id.autoTxtV_shopping_list_item_name);
        llItemHolder    = (LinearLayout)fragment.findViewById(R.id.ll_shopping_list_row_holder);
        llSuggestions   = (LinearLayout)fragment.findViewById(R.id.ll_shopping_list_suggestion_row_holder);
        //actions
        this.btnAddItem.setOnClickListener(this);
        btnClear.setOnClickListener(clear);
    }

    private void restoreShoppingList(){
        this.llItemHolder.removeAllViews();
        shoppingListTable   = new ShoppingListTable(getActivity().getApplicationContext());
        shoppingListTable.open();
        ArrayList <ShoppingListItemHolder> shl = shoppingListTable.selectAll();

        for(int i = 0; i < shl.size(); i++){
            insertItem(shl.get(i).getItemId());
        }

    }

    private void initDB(){
        this.itemTable = new ItemTable(getActivity().getApplicationContext());
        //this.shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
        //this.allPurchasesTable = new AllPurchasesTable(getActivity().getApplicationContext());
        Log.d("lel", "durr");
        initHints();
        initMostBought();
    }
    private void initMostBought(){

        AllPurchasesTable apt = new AllPurchasesTable(getActivity().getApplicationContext());
        apt.open();
        ArrayList<AllPurchasesCount> apc = apt.selectMostCommon(10);
        apt.close();
        itemTable = new ItemTable(getActivity().getApplicationContext());
        itemTable.open();
        ArrayList<ItemHolder>itemHolders = itemTable.selectAll();
        if(apc.size() > 0)
            llSuggestions.removeAllViews();
        for(int i = 0; i < apc.size(); i++){

            String itemName = "";
            String itemInfo = "";
            int id = 0;


            for(int j = 0; j < itemHolders.size(); j++){
                if (apc.get(i).getId() == itemHolders.get(j).getId()){
                    id = (int)itemHolders.get(j).getId();
                    itemName = itemHolders.get(j).getName();
                    itemInfo = "" + apc.get(i).getCount();
                    Log.d("dorp", "jag kommer ju hit");
                    break;
                }
            }

            if(id > 0)
                adMostBoughtItems(itemName, itemInfo, id);
        }

    }
    private void initHints(){
        items = new ArrayList<ItemHolder>();
        itemNames = new ArrayList<String>();
        try{
            itemTable.open();
            //long l = itemTable.insertItem(new ItemHolder(1, "banan"));
            items = itemTable.selectAll();
        }catch (SQLiteException e){
            Log.d("lel", e.getMessage());
        }catch (Exception e){
            Log.e("lel", e.getMessage());
        }
        finally {
            itemTable.close();
        }

        for (int i = 0; i < items.size(); i++){
            itemNames.add(items.get(i).getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                itemNames);
        acTxt.setAdapter(adapter);
    }


    //should have a parameter for the Object with information too
    public void insertItem(long itemId){
        try{
            Log.d("derp", "trying to insert item");
            itemTable = new ItemTable(getActivity().getApplicationContext());
            itemTable.open();
            final ItemHolder ih = itemTable.selectItem(itemId);
            itemTable.close();
            shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
            shoppingListTable.open();
            boolean isBought = shoppingListTable.select(itemId).isBought();
            final ShoppingListItemHolder shoppingListItemHolder = shoppingListTable.select(itemId);
            shoppingListTable.close();
            Log.d("derp", "--trying to insert item");

            View row    = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.shopping_list_row, null);
            TextView txtId = (TextView)row.findViewById(R.id.txtV_list_row_id);
            TextView txtTitle =(TextView)row.findViewById(R.id.txtV_list_row_title);
            TextView txtAmount =(TextView)row.findViewById(R.id.txtV_list_row_amount);
            CheckBox checkBox = (CheckBox)row.findViewById(R.id.chb_list_row_done);
            ImageView img = (ImageView)row.findViewById(R.id.imgV_list_row);
            final LinearLayout llEdit = (LinearLayout)row.findViewById(R.id.ll_shopping_list_row_edit);

            Log.d("derp", "---trying to insert item");
            if(isBought)
                checkBox.setChecked(true);
            txtId.setText(String.valueOf(itemId));
            txtTitle.setText(ih.getName());
            txtAmount.setText(ih.getAmount() + " " + ih.getComment());
            Log.d("derp", "----trying to insert item");

            if(ih != null && ih.getPicture().length() > 0){
                Log.d("derp", "setting uri");
                Uri uri = Uri.parse(ih.getPicture());
                if(uri != null){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    Bitmap bp = BitmapFactory.decodeFile(ih.getPicture(),options);

                    options.inSampleSize = 6;
                    options.inJustDecodeBounds = false;
                    bp = BitmapFactory.decodeFile(ih.getPicture(),options);
//            bp = Bitmap.createBitmap(bp, 0, 0, 100, 100);
                    //img.setScaleType(ImageView.ScaleType.CENTER);
                    img.setImageBitmap(bp);
                    //img.setImageURI(uri);
                    //img.setScaleType(ImageView.ScaleType.CENTER);
                }
            }
            Log.d("derp", "-----trying to insert item");

            final EditText etName     = (EditText)row.findViewById(R.id.et_shopping_list_row_title);
            final EditText etAmount   = (EditText)row.findViewById(R.id.et_shopping_list_row_amount);
            final EditText etComment  = (EditText)row.findViewById(R.id.et_shopping_list_row_comment);
            Button btnSave            = (Button)row.findViewById(R.id.btn_shopping_list_row_save);
            Button btnDelete          = (Button)row.findViewById(R.id.btn_shopping_list_row_remove);


            etName.setText(ih.getName());
            etAmount.setText(ih.getAmount());
            etComment.setText(ih.getComment());

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        try{
                        shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
                        shoppingListTable.open();
                        ShoppingListItemHolder shoppingListItemHolder = shoppingListTable.select(ih.getId());
                        shoppingListItemHolder.setBought(true);
                        shoppingListTable.update(shoppingListItemHolder);
                        shoppingListTable.close();
                        }catch (SQLiteException e){
                            Log.e("err_mess", e.getLocalizedMessage());
                        }catch (Exception e){
                            Log.e("err_mess", e.getMessage());
                        }
                    }else{
                        try{
                            shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
                            shoppingListTable.open();
                            ShoppingListItemHolder shoppingListItemHolder = shoppingListTable.select(ih.getId());
                            shoppingListItemHolder.setBought(false);
                            shoppingListTable.update(shoppingListItemHolder);
                            shoppingListTable.close();
                        }catch (SQLiteException e){
                            Log.e("err_mess", e.getLocalizedMessage());
                        }catch (Exception e){
                            Log.e("err_mess", e.getMessage());
                        }
                    }
                }
            });

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(llEdit.getVisibility() == View.GONE)
                        llEdit.setVisibility(View.VISIBLE);

                    else{
                        llEdit.setVisibility(View.GONE);
                        ItemHolder holder = new ItemHolder(ih.getId(),
                                etName.getText().toString(),
                                etComment.getText().toString(),
                                etAmount.getText().toString(),
                                ih.getPicture());
                        Log.d("dorp", holder.getName());
                        updateItem(holder);
                        restoreShoppingList();
                    }
                }
            });
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("dorp", shoppingListItemHolder.getId() + " is id");
                    llEdit.setVisibility(View.GONE);
                    ItemHolder holder = new ItemHolder(ih.getId(),
                            etName.getText().toString(),
                            etComment.getText().toString(),
                            etAmount.getText().toString(),
                            ih.getPicture());

                    Log.d("dorp", holder.getName());
                    updateItem(holder);
                    restoreShoppingList();
                }
            });
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("dorp", shoppingListItemHolder.getId() + " is the id");
                    llEdit.setVisibility(View.GONE);
                    ItemHolder holder = new ItemHolder(ih.getId(),
                            etName.getText().toString(),
                            etComment.getText().toString(),
                            etAmount.getText().toString());


                    deleteItem(shoppingListItemHolder.getId());
                    restoreShoppingList();
                }
            });
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity().getApplicationContext(), "image clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity().getApplicationContext(), ItemSettingsActivity.class);
                    intent.putExtra(Consts.ITEM_ID, ih.getId());
                    startActivity(intent);
                }
            });


            this.llItemHolder.addView(row, 0);
            Log.d("derp", "------trying to insert item");

        }catch (SQLiteException e){
            Log.e("err_mess", e.getMessage() + e.getLocalizedMessage());
            return;
        }

    }
    private void updateItem(ItemHolder itemHolder){
        try{
        itemTable = new ItemTable(getActivity().getApplicationContext());
        itemTable.open();
        itemTable.update(itemHolder);
        itemTable.close();
        }catch (SQLiteException e){
            Log.e("err_mess", e.getMessage());
            Log.e("err_mess", e.getLocalizedMessage());
        }
    }

    private void deleteItem(long l){
        try{
            shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
            shoppingListTable.open();
            shoppingListTable.delete(l);
            shoppingListTable.close();
        }catch (SQLiteException e){
            Log.e("err_mess", e.getMessage());
            Log.e("err_mess", e.getLocalizedMessage());
        }
    }

    private String getTxtvItemName(){
        return acTxt.getText().toString();
    }


    private void adMostBoughtItems(String name, String info, int id){
        View row    = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.stats_list_row, null);
        TextView txtName    = (TextView)row.findViewById(R.id.txtV_stats_row_name);
        TextView txtInfo    = (TextView)row.findViewById(R.id.txtV_stats_row_info);
        TextView txtId      = (TextView)row.findViewById(R.id.txtV_stats_row_id);

        txtName.setText(name);
        txtInfo.setText(info);
        txtId.setText(String.valueOf(id));
        row.setOnClickListener(onMostClicked);
        llSuggestions.addView(row);
    }

    public void saveState(){

    }

    View.OnClickListener onMostClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView tvId = (TextView)v.findViewById(R.id.txtV_stats_row_id);
            int id = Integer.parseInt(tvId.getText().toString());

            //add item to shopping list
            shoppingListTable = new ShoppingListTable(getActivity().getApplicationContext());
            shoppingListTable.open();
            shoppingListTable.insert(new ShoppingListItemHolder(id));
            shoppingListTable.close();
            restoreShoppingList();
        }
    };

    View.OnClickListener clear = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            //Open dialogue
            String[] arr = {"Yes", "I only bought the checked items", "Just clear the list", "Cancel"};
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(R.string.alert_shopping_list_title);
            alert.setItems(arr, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            //save all items to history
                            shoppingListTable = new ShoppingListTable(getActivity());
                            shoppingListTable.open();

                            ArrayList<ShoppingListItemHolder> list = shoppingListTable.selectAll();
                            ArrayList<ShoppingListItemHolder> newList = new ArrayList<ShoppingListItemHolder>();
                            for(int j = 0; j < list.size(); j++){

                                    newList.add(list.get(j));
                            }
                            shoppingListTable.recreate();
                            shoppingListTable.close();
                            llItemHolder.removeAllViews();
                            allPurchasesTable = new AllPurchasesTable(getActivity().getApplicationContext());
                            allPurchasesTable.open();
                            for(int k = 0; k < newList.size(); k++){
                                allPurchasesTable.insert(new AllPurchasesHolder(newList.get(k).getItemId(), ""));
                            }
                            ArrayList<AllPurchasesHolder> allPurchasesHolders = allPurchasesTable.selectAll();
                            allPurchasesTable.close();


                            shoppingListTable = new ShoppingListTable(getActivity());
                            shoppingListTable.open();
                            shoppingListTable.recreate();
                            llItemHolder.removeAllViews();
                            initMostBought();
                            break;
                        case 1:
                            //save only selected items
                            shoppingListTable = new ShoppingListTable(getActivity());
                            shoppingListTable.open();
                            ArrayList<ShoppingListItemHolder> list2 = shoppingListTable.selectAll();
                            ArrayList<ShoppingListItemHolder> newList2 = new ArrayList<ShoppingListItemHolder>();
                            for(int j = 0; j < list2.size(); j++){
                                if(list2.get(j).isBought())
                                    newList2.add(list2.get(j));
                            }
                            //new way
                            for(int k = 0; k < newList2.size(); k++){
                                shoppingListTable.delete(newList2.get(k).getId());
                            }
                            shoppingListTable.close();
                            llItemHolder.removeAllViews();

                            allPurchasesTable = new AllPurchasesTable(getActivity().getApplicationContext());
                            allPurchasesTable.open();
                            for(int k = 0; k < newList2.size(); k++){
                                allPurchasesTable.insert(new AllPurchasesHolder(newList2.get(k).getItemId(), ""));
                            }
                            ArrayList<AllPurchasesHolder> allPurchasesHolders2 = allPurchasesTable.selectAll();
                            allPurchasesTable.close();

                            for(int g = 0; g < allPurchasesHolders2.size(); g++)
                                Log.d("dorp", "lel " + allPurchasesHolders2.get(g).getTimeStamp());

                            restoreShoppingList();
                            initMostBought();
                            break;
                        case 2:
                            shoppingListTable = new ShoppingListTable(getActivity());
                            shoppingListTable.open();
                            shoppingListTable.recreate();
                            llItemHolder.removeAllViews();
                            initMostBought();
                            break;
                    }
                }
            });

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
    };


}
