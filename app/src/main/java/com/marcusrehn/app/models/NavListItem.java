package com.marcusrehn.app.models;

/**
 * Created by rehnen on 2014-01-31.
 */
public class NavListItem {
    private String  title;
    private String  message;
    private int     iconId;
    private boolean visible = false;


    public NavListItem(){

    }
    public NavListItem(String title, String message, int iconId, boolean visible){
        this.title = title;
        this.message = message;
        this.iconId = iconId;
        this.visible = visible;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
