package com.marcusrehn.app;

import android.animation.Animator;
import android.app.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.marcusrehn.app.database.tables.AllPurchasesTable;
import com.marcusrehn.app.database.tables.ItemTable;
import com.marcusrehn.app.database.tables.ShoppingListTable;

/**
 * Created by rehnen on 2014-04-03.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {

    Button btnClearHist;
    Button btnClearAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        btnClearHist = (Button) view.findViewById(R.id.btn_settings_clear_history);
        btnClearAll = (Button) view.findViewById(R.id.btn_settings_clear_all_data);

        btnClearHist.setOnClickListener(this);
        btnClearAll.setOnClickListener(this);

        return view;

    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_settings_clear_history:
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Are you sure?").setPositiveButton("Yes", alertClickClearHist)
                        .setNegativeButton("No", alertClickClearHist).show();

                break;
            case R.id.btn_settings_clear_all_data:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(view.getContext());
                builder2.setMessage("Are you sure?").setPositiveButton("Yes", alertClickClearAll)
                        .setNegativeButton("No", alertClickClearAll).show();
                break;
        }
    }
    DialogInterface.OnClickListener alertClickClearHist = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(which == DialogInterface.BUTTON_POSITIVE){
                try{
                    AllPurchasesTable apt = new AllPurchasesTable(getActivity().getApplicationContext());
                    apt.open();
                    apt.recreate();
                    apt.close();
                    Toast.makeText(getActivity().getApplicationContext(), "All clear", Toast.LENGTH_SHORT).show();
                }catch (SQLiteException e){
                    Log.e("mmm", ".");
                    e.printStackTrace();
                }catch (Exception e){
                    Log.e("mmm", ".");
                    e.printStackTrace();
                }
            }else if(which == DialogInterface.BUTTON_NEGATIVE){
                Toast.makeText(getActivity().getApplicationContext(), "No changes were made", Toast.LENGTH_SHORT).show();
            }
        }
    };
    DialogInterface.OnClickListener alertClickClearAll = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(which == DialogInterface.BUTTON_POSITIVE){
                try{
                    AllPurchasesTable apt = new AllPurchasesTable(getActivity().getApplicationContext());
                    apt.open();
                    apt.recreate();
                    apt.close();

                    ShoppingListTable slt = new ShoppingListTable(getActivity().getApplicationContext());
                    slt.open();
                    slt.recreate();
                    slt.close();

                    ItemTable it = new ItemTable(getActivity().getApplicationContext());
                    it.open();
                    it.remakeTable();
                    it.close();
                    Toast.makeText(getActivity().getApplicationContext(), "All clear", Toast.LENGTH_SHORT).show();
                }catch (SQLiteException e){
                    Log.e("mmm", ".");
                    e.printStackTrace();
                }catch (Exception e){
                    Log.e("mmm", ".");
                    e.printStackTrace();
                }
            }else if(which == DialogInterface.BUTTON_NEGATIVE){
                Toast.makeText(getActivity().getApplicationContext(), "No changes were made", Toast.LENGTH_SHORT).show();
            }
        }
    };
}
