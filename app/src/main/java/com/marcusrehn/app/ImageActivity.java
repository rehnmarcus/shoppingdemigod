package com.marcusrehn.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.FileNotFoundException;

/**
 * Created by rehnen on 2014-04-02.
 */
public class ImageActivity extends Activity {

    public static final String FLAG_PIC = "pic_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.activity_image, null);
        setContentView(view);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras == null)
            return;

        try{
        String picUrl = extras.getString(FLAG_PIC);
        ImageView img = (ImageView)view.findViewById(R.id.imgV_activity_image);
        Bitmap bm = BitmapFactory.decodeFile(picUrl);
        img.setImageBitmap(bm);
        }catch (Exception e){
            Log.e("err_lel", e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
