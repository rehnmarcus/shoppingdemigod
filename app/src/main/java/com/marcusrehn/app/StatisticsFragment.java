package com.marcusrehn.app;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marcusrehn.app.database.holders.AllPurchasesCount;
import com.marcusrehn.app.database.holders.AllPurchasesHolder;
import com.marcusrehn.app.database.holders.ItemHolder;
import com.marcusrehn.app.database.tables.AllPurchasesTable;
import com.marcusrehn.app.database.tables.ItemTable;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by rehnen on 2014-03-24.
 */
public class StatisticsFragment extends Fragment {


    //private AutoCompleteTextView acTxt;
    //private Button btnSearch;

    private LinearLayout llMostBought;
    private LinearLayout llThingsYouNeed;

    private ArrayList<ItemHolder> items;
    private ArrayList<String> itemNames;

    private ItemTable itemTable;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_statistics, container, false);

        initGUI(v);

        ActionBar actionBar = getActivity().getActionBar();

        initHints();


        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        super.onResume();
        initMightWant();
        initMostBought();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }




    private void initGUI(View v){
        //this.acTxt              = (AutoCompleteTextView) v.findViewById(R.id.act_statistics);
        //this.btnSearch          = (Button) v.findViewById(R.id.btn_stats_search);
        this.llMostBought       = (LinearLayout) v.findViewById(R.id.ll_stats_top_items);
        this.llThingsYouNeed    = (LinearLayout) v.findViewById(R.id.ll_stats_what_you_want);

        //btnSearch.setOnClickListener(onSearchClicked);

        //adMostBoughtItems("name", "info", 1);
        //adMostBoughtItems("name", "info", 2);
        //adMostBoughtItems("name", "info", 3);
        initMostBought();
        initMightWant();

    }

    private void initHints(){
        items = new ArrayList<ItemHolder>();
        itemNames = new ArrayList<String>();
        itemTable = new ItemTable(getActivity().getApplicationContext());
        try{
            itemTable.open();
            //long l = itemTable.insertItem(new ItemHolder(1, "banan"));
            items = itemTable.selectAll();
        }catch (SQLiteException e){
            Log.d("lel", e.getMessage());
        }catch (Exception e){
            Log.e("lel", e.getMessage());
        }
        finally {
            itemTable.close();
        }

        for (int i = 0; i < items.size(); i++){
            itemNames.add(items.get(i).getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                itemNames);
        //acTxt.setAdapter(adapter);
    }
    private void initMightWant(){
        AllPurchasesTable apt = new AllPurchasesTable(getActivity().getApplicationContext());
        apt.open();
        ArrayList<AllPurchasesHolder> aph = apt.selectLatest(20, 100);
        apt.close();
        itemTable = new ItemTable(getActivity().getApplicationContext());
        itemTable.open();
        ArrayList<ItemHolder>itemHolders = itemTable.selectAll();
        if(aph.size() > 0)
            llThingsYouNeed.removeAllViews();
        for(int i = 0; i < aph.size(); i++){

            String itemName = "";
            String itemInfo = "";
            int id = 0;


            for(int j = 0; j < itemHolders.size(); j++){
                if (aph.get(i).getItemId() == itemHolders.get(j).getId()){
                    id = (int)itemHolders.get(j).getId();
                    itemName = itemHolders.get(j).getName();
                    itemInfo = aph.get(i).getTimeStamp();
                    Log.d("dorp", "jag kommer ju hit");
                    break;
                }
            }

            if(id > 0)
                adMightWantItem(itemName, itemInfo, id);
        }
    }
    private void initMostBought(){

        AllPurchasesTable apt = new AllPurchasesTable(getActivity().getApplicationContext());
        apt.open();
        ArrayList<AllPurchasesCount> apc = apt.selectMostCommon(10);
        apt.close();
        itemTable = new ItemTable(getActivity().getApplicationContext());
        itemTable.open();
        ArrayList<ItemHolder>itemHolders = itemTable.selectAll();
        if(apc.size() > 0)
            llMostBought.removeAllViews();
        for(int i = 0; i < apc.size(); i++){

            String itemName = "";
            String itemInfo = "";
            int id = 0;


            for(int j = 0; j < itemHolders.size(); j++){
                if (apc.get(i).getId() == itemHolders.get(j).getId()){
                    id = (int)itemHolders.get(j).getId();
                    itemName = itemHolders.get(j).getName();
                    itemInfo = "" + apc.get(i).getCount();
                    Log.d("dorp", "jag kommer ju hit");
                    break;
                }
            }

            if(id > 0)
                adMostBoughtItems(itemName, itemInfo, id);
        }

    }

    private void adMostBoughtItems(String name, String info, int id){
        View row    = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.stats_list_row, null);
        TextView txtName    = (TextView)row.findViewById(R.id.txtV_stats_row_name);
        TextView txtInfo    = (TextView)row.findViewById(R.id.txtV_stats_row_info);
        TextView txtId      = (TextView)row.findViewById(R.id.txtV_stats_row_id);

        txtName.setText(name);
        txtInfo.setText(info);
        txtId.setText(String.valueOf(id));
        row.setOnClickListener(onMostClicked);
        llMostBought.addView(row);
    }

    private void adMightWantItem(String name, String info, int id){
        View row    = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.stats_list_row, null);
        TextView txtName    = (TextView)row.findViewById(R.id.txtV_stats_row_name);
        TextView txtInfo    = (TextView)row.findViewById(R.id.txtV_stats_row_info);
        TextView txtId      = (TextView)row.findViewById(R.id.txtV_stats_row_id);

        txtName.setText(name);
        txtInfo.setText(info);
        txtId.setText(String.valueOf(id));
        row.setOnClickListener(onWantClicked);
        llThingsYouNeed.addView(row);
    }




    //Click listeners
    /*private View.OnClickListener onSearchClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            itemTable = new ItemTable(getActivity().getApplicationContext());
            itemTable.open();
            ItemHolder ih = itemTable.selectItem(acTxt.getText().toString());
            if(ih == null)
                return;

            Intent startItem = new Intent();
            startItem.setClass(getActivity(), ItemSettingsActivity.class);
            startItem.putExtra(Consts.ITEM_ID, ih.getId());
            startActivity(startItem);

        }
    };*/

    private View.OnClickListener onMostClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TextView txtId = (TextView)view.findViewById(R.id.txtV_stats_row_id);
            Log.d("clicked", "id = " + txtId.getText().toString());
            Intent startItem = new Intent();
            startItem.setClass(getActivity(), ItemSettingsActivity.class);
            startItem.putExtra(Consts.ITEM_ID, Long.valueOf(txtId.getText().toString()));
            startActivity(startItem);
        }
    };

    private View.OnClickListener onWantClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TextView txtId = (TextView)view.findViewById(R.id.txtV_stats_row_id);
            Log.d("clicked", "id = " + txtId.getText().toString());
            Intent startItem = new Intent();
            startItem.setClass(getActivity(), ItemSettingsActivity.class);
            startItem.putExtra(Consts.ITEM_ID, Long.valueOf(txtId.getText().toString()));
            startActivity(startItem);
        }
    };

    private void seekThatItem(){

    }
}
