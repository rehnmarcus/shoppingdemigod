package com.marcusrehn.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.marcusrehn.app.database.holders.ItemHolder;
import com.marcusrehn.app.database.tables.ItemTable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by rehnen on 2014-03-27.
 */
public class ItemSettingsActivity extends Activity {

    public static final int GALLERY_REQUEST = 0;
    public static final int CAMERA_REQUEST = 1;

    private long id;

    private EditText eTxtName;
    private EditText eTxtAmount;
    private EditText eTxtComment;
    private ImageView imgV;
    private Button btnSave;
    private Button btnDelete;

    private final Context context = this;

    private String pictureUrl = "";

    @Override
    public void onBackPressed() {

        try{
            ItemTable it = new ItemTable(getApplicationContext());
            it.open();
            ItemHolder ih = it.selectItem(id);
            it.update(new ItemHolder(id,
                    eTxtName.getText().toString(),
                    eTxtComment.getText().toString(),
                    eTxtAmount.getText().toString(),
                    ih.getPicture()));
            it.close();

        }catch (SQLiteException e){
            Log.e("sqlite err", e.getLocalizedMessage());
        }catch (Exception e){
            Log.e("sqlite err not caought", e.toString());
        }

        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.activity_item_settings, null);
        setContentView(view);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setIcon(R.drawable.settings);

        Bundle extras = getIntent().getExtras();
        if(extras == null)
            return;

        id = extras.getLong(Consts.ITEM_ID);
        Log.d("out", "id = " + id);

        initGUI(view);
        initDB();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == GALLERY_REQUEST){
            Uri targetUri = data.getData();
            Bitmap bitmap;
            try{
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(targetUri));
                imgV.setImageBitmap(bitmap);
                //ContentResolver cr = context.getContentResolver();
                //InputStream is = cr.openInputStream(targetUri);
                //Uri.parse("");
                //targetUri = getContentResolver().openInputStream()
                String[] proj = { MediaStore.Images.Media.DATA };
                Cursor cursor = managedQuery(targetUri, proj, null, null, null);
                int column_index
                        = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                cursor.getString(column_index);

                pictureUrl = cursor.getString(column_index);
                Log.d("dorp", cursor.getString(column_index));
                Log.d("dorp pic", pictureUrl);
                ItemTable it = new ItemTable(getApplicationContext());
                it.open();
                it.update(new ItemHolder(id, eTxtName.getText().toString(),
                        eTxtComment.getText().toString(), eTxtAmount.getText().toString(), pictureUrl));
                it.close();
            }
            catch (SQLiteException e){
                Log.e("err--lel1msg", "sql flipout");
                if(e.getLocalizedMessage()!=null)
                    Log.e("err--lel1msg", e.getLocalizedMessage());
            }
            catch (FileNotFoundException e){
                Log.e("err", e.getLocalizedMessage());
            }
        }
        else if(resultCode == RESULT_OK && requestCode == CAMERA_REQUEST){
            Uri targetUri = data.getData();
            Bitmap bitmap;
            try{
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                //imgV.setImageBitmap(thumbnail);
                try{
                    File saveDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/shoppingdemigod");
                    boolean success = true;
                    if(!saveDir.exists()){
                        success = saveDir.mkdir();
                    }
                    if(success){
                        FileOutputStream fo = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath()+
                                "/shoppingdemigod/"+ id + ".jpg");
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, fo);
                        Bitmap bp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath()+
                                "/shoppingdemigod/"+ id + ".jpg");
                        imgV.setImageBitmap(bp);
                        Log.d("dorp", Environment.getExternalStorageDirectory().getAbsolutePath());

                        pictureUrl = Environment.getExternalStorageDirectory().getAbsolutePath()+
                                "/shoppingdemigod/"+ id + ".jpg";
                        ItemTable it = new ItemTable(getApplicationContext());
                        it.open();
                        it.update(new ItemHolder(id, eTxtName.getText().toString(),
                                eTxtComment.getText().toString(), eTxtAmount.getText().toString(), pictureUrl));
                        it.close();
                    }else {
                        Toast.makeText(getApplicationContext(), "Unable so save image", Toast.LENGTH_LONG);
                    }





                }
                catch (SQLiteException e){
                    Log.e("err--lel1msg", "sql flipout");
                    if(e.getLocalizedMessage()!=null)
                        Log.e("err--lel1msg", e.getLocalizedMessage());
                }
                catch (Exception e){
                    Log.e("err--lel1", "shit");
                    if(e.getLocalizedMessage()!=null)
                        Log.e("err--lel1msg", e.getLocalizedMessage());
                    e.printStackTrace();
                }

            }catch (Exception e){
                Log.e("err--lel2", e.toString() + "duh shit");
                e.printStackTrace();
            }
        }
    }

    private void initGUI(View v){
        eTxtName = (EditText)v.findViewById(R.id.et_item_settings_name);
        eTxtAmount = (EditText)v.findViewById(R.id.et_item_settings_amount);
        eTxtComment = (EditText)v.findViewById(R.id.et_item_settings_comment);
        btnSave = (Button)v.findViewById(R.id.btn_item_settings_save);
        btnDelete = (Button)v.findViewById(R.id.btn_item_settings_delete);
        imgV = (ImageView)v.findViewById(R.id.imgV_item_settings);

        btnSave.setOnClickListener(onClickSave);
        btnDelete.setOnClickListener(onClickDelete);
        imgV.setOnClickListener(onClickImgV);
    }


    private void initDB(){
        try{
            ItemTable it = new ItemTable(getApplicationContext());
            it.open();
            ItemHolder ih = it.selectItem(this.id);

            eTxtName.setText(ih.getName());
            eTxtAmount.setText(ih.getAmount());
            eTxtComment.setText(ih.getComment());
            pictureUrl = ih.getPicture();

            /*

            I need to fix the image view as well

             */
            if(ih.getPicture() == null || ih.getPicture().length() == 0)
                return;

            //this is really wierd
            //http://stackoverflow.com/questions/15377186/decode-file-from-sdcard-android-to-avoid-out-of-memory-error-due-to-large-bitmap
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bp = BitmapFactory.decodeFile(ih.getPicture(),options);

            options.inSampleSize = calculateInSampleSize(options, 2000, 1000);
            options.inJustDecodeBounds = false;
            bp = BitmapFactory.decodeFile(ih.getPicture(),options);
//            bp = Bitmap.createBitmap(bp, 0, 0, 100, 100);
            //imgV.setScaleType(ImageView.ScaleType.CENTER);
            imgV.setImageBitmap(bp);


        }catch (SQLiteException e){
            Log.e("err", e.getLocalizedMessage());
        }

    }


    private void saveImageChange(String url){

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }



    private View.OnClickListener onClickSave = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try{
                ItemTable it = new ItemTable(getApplicationContext());
                it.open();
                ItemHolder ih = it.selectItem(id);
                it.update(new ItemHolder(id,
                        eTxtName.getText().toString(),
                        eTxtComment.getText().toString(),
                        eTxtAmount.getText().toString(),
                        ih.getPicture()));
                it.close();

            }catch (SQLiteException e){
                Log.e("sqlite err", e.getLocalizedMessage());
            }catch (Exception e){
                Log.e("sqlite err not caought", e.toString());
            }

        }
    };


    private View.OnClickListener onClickDelete = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try{
                ItemTable it = new ItemTable(getApplicationContext());
                it.open();
                ItemHolder ih = it.selectItem(id);
                it.deleteItem(ih);
                it.close();
            }catch (SQLiteException e){
                Log.e("sqlite err", e.getLocalizedMessage());
            }catch (Exception e){
                Log.e("sqlite err not caought", e.toString());
            }

        }
    };

    private View.OnClickListener onClickImgV = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String[] arr = {"Select from phone", "New picture", "Display image"};

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.alert_shopping_list_title);
            builder.setItems(arr,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i) {
                                case 0:
                                    Intent intent = new Intent(Intent.ACTION_PICK,
                                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(intent, GALLERY_REQUEST);
                                    break;
                                case 1:
                                    Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    Uri uriSavedImage=Uri.fromFile(new File("/sdcard/flashCropped.png"));
                                    //intent2.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                                    if (intent2.resolveActivity(getPackageManager()) != null) {
                                        startActivityForResult(intent2, CAMERA_REQUEST);
                                    }
                                    break;
                                case 2:
                                    if(pictureUrl.length() > 0){
                                        Intent intent3 = new Intent(getApplicationContext(), ImageActivity.class);
                                        intent3.putExtra(ImageActivity.FLAG_PIC, pictureUrl);
                                        startActivity(intent3);
                                        Log.d("dorp pic lel", pictureUrl);

                                    }else{
                                        Toast.makeText(getApplicationContext(), "No image set", Toast.LENGTH_SHORT);
                                    }
                                    break;
                            }
                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    };
}
