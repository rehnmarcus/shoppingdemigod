package com.marcusrehn.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcusrehn.app.R;
import com.marcusrehn.app.models.NavListItem;

import java.util.ArrayList;

/**
 * Created by rehnen on 2014-01-31.
 */

//A good read on the topic is available at:
//http://www.androidhive.info/2013/11/android-sliding-menu-using-navigation-drawer/
public class NavListItemAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<NavListItem> navList;
    public NavListItemAdapter(Context context, ArrayList<NavListItem> navList){
        this.context = context;
        this.navList = navList;
    }

    @Override
    public int getCount() {
        return navList.size();
    }

    @Override
    public NavListItem getItem(int i) {
        return navList.get(i);
    }
    //should probably do something smarter with that one
    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.nav_list_item, null);
        }
        NavListItem navListItem = navList.get(i);
        ImageView icon      = (ImageView) view.findViewById(R.id.imgV_nav_list_item_icon);
        TextView title      = (TextView) view.findViewById(R.id.txtV_nav_list_item_title);
        TextView message    = (TextView) view.findViewById(R.id.txtV_nav_list_item_note);





        title.setText(navListItem.getTitle());
        message.setText(navListItem.getMessage());
        icon.setImageResource(navListItem.getIconId());
        //icon.setDrawableResource(new Drawable(icon.getId()));navListItem.getIconId();

        if(navList.get(i).isVisible()) {
            message.setVisibility(View.VISIBLE);
        }else{
            message.setVisibility(View.GONE);
        }

        return view;
    }
}
